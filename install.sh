#!/usr/bin/bash

echo Starting installation of \`nvim-edit\`...
local_bin_directory="$HOME/.local/bin"

mkdir -p "$local_bin_directory" # create local bin directory if needed

cp ./nvim-edit "$local_bin_directory"
echo Copied \"`pwd`/nvim-edit\" to \"$local_bin_directory/nvim-edit\"
echo Finished installation of \`nvim-edit\`!
echo
echo Done!
echo
